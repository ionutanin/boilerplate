const gulp = require('gulp'),
    less = require('gulp-less'),
    sync = require('browser-sync').create(),
    mini = require('gulp-clean-css'),
    maps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    inject = require('gulp-inject'),
    gulpif = require('gulp-if'),
    pug = require('gulp-pug'),
    prefix = require('gulp-autoprefixer'),
    notify = require('gulp-notify');

const config = {
    'styles': {
        'src': ['src/assets/**/*.less', 'src/components/**/*.less', 'src/pages/**/*.less'],
        'index':'src/assets/index.less',
        'dest': 'dist/css',
        'bundle_name': 'all.min.css',
        'minify': false,
        'create_source_map':true
    }
};

const lessTask = () => gulp.src(config.styles.index)
    .pipe(inject(gulp.src(config.styles.src, {read: false, cwd: ''}), {
        starttag: '/* inject:imports */',
        endtag: '/* endinject */',
        transform: filepath => `@import '.${filepath}';`
    }))
    .pipe(gulpif(config.styles.create_source_map, maps.init()))
    .pipe(less()).on('error', notify.onError(error => error))
    .pipe(mini())
    .pipe(concat(config.styles.bundle_name))
    .pipe(prefix())
    .pipe(gulpif(config.styles.create_source_map, maps.write('./')))
    .pipe(gulp.dest(config.styles.dest))
    .pipe(sync.stream({match: '**/*.css'}));

const pugTask = done => {
    gulp.src(['./src/pages/**/*.pug'])
        .pipe(pug({pretty: true})).on('error', notify.onError(function (error) {
            return error;
        }))
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest('dist'));
    done();
};

const reload = done => {
    sync.reload();
    done();
};

const imagesTask = done => {
    gulp.src(['./src/assets/img/**/*.*']).pipe(gulp.dest('dist/img'));
    done();
};

const fontsTask = done => {
    gulp.src(['./src/assets/fonts/**/*.*']).pipe(gulp.dest('dist/fonts'));
    done();
};

const syncTask = done => {
    sync.init({ server: './dist/' });
    done();
};

const watchImages = done => {
    gulp.watch('./src/assets/img/**/*.*', gulp.series(imagesTask, reload));
    done();
};

const watchFonts = done => {
    gulp.watch('./src/assets/fonts/**/*.*', gulp.series(fontsTask, reload));
    done();
};

const watchLess = done => {
    gulp.watch('./src/**/*.less', gulp.series(lessTask, reload));
    done();
};

const watchPug = done => {
    gulp.watch('./src/**/*.pug', pugTask);
    done();
};

const watchHtml = done => {
    gulp.watch('./dist/*.html').on('change', sync.reload);
    done();
};

exports.default = gulp.series(
    lessTask,
    pugTask,
    imagesTask,
    fontsTask
);

exports.watch = gulp.series(
    exports.default,
    syncTask,
    watchImages,
    watchFonts,
    watchLess,
    watchPug,
    watchHtml
);
